# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 23:12:43 2015

@author: pasha
"""
# Стандартные модули
import os, re, sys
import operator #, webbrowser

# Tkinter
import Tkinter as tk
from Tkinter import Button, Entry, Frame, Label, Listbox, Menu, Text, Tk 
from Tkinter import BOTH, LEFT, RIGHT, END, StringVar, W, E 
import tkMessageBox, tkFileDialog, tkFont

# bibtexparser
import bibtexparser
from bibtexparser.bibdatabase import BibDatabase

from docx import Document


class RuBib(Frame):
  
    def __init__(self, parent):
        
        Frame.__init__(self, parent)            
        self.parent = parent        
        self.initUI()
        
    def initUI(self):
        '''
        Формирование главного окна приложения 
        '''
        self.progname = 'RuBib'
        self.version = '1.1.0'
        self.journals_list = [u"Метеорология и гидрология",
#        u"Доклады академии наук",
#        u"Океанология",
#        u"Лёд и снег",
#        u"Вестник МГУ. Серия географическая",
#        u"Journal of climate"]
        u"ГОСТ",]
        self.journals_list.sort()

        N = 50
        LN = 20 # bib's labels width
        ent_colspan = 1
        n = 2
        # Шрифты        
        tahoma_bold = tkFont.Font(family='Tahoma', size=10, weight='bold')
        p14 = tkFont.Font(family='Tahoma', size=10) # you don't have to use Helvetica or bold, this is just an example
                
        self.dic = {'author': [u"Автор(ы)/Author(s)", n+1],
                    'title':[u"Название/Title", n+2],
                    'booktitle':[u"Название книги/Booktitle", n+3],
                    'year':[u"Год/Year", n+4],
                    'pages':[u"Страницы/Pages", n+5],
                    'journal':[u"Журнал/Journal", n+6],
                    'number':[u"Номер/Number", n+7],
                    'volume':[u"Том/Volume", n+8],
                    'publisher':[u"Издатель/Publisher", n+9],
                    'address':[u"Город/Address", n+10],
                    'url':[u"Режим доступа/Url", n+11],
                    'doi':[u"DOI", n+12],
                    'issn':[u"ISSN", n+13],
                    'language':[u"Язык/Language", n+14], 
                    'note':[u"Примечания/Note", n+15], 
                    'ID':[ u"Аббревиатура/ID", n+16],
                    'ENTRYTYPE': [u"Тип документа/Entrytype", n+17]
                              }
        dkeys = self.dic.keys()
        dkeys.sort()
#        print dkeys
      
        self.parent.title("Submenu")
        self.biblist = []
        self.search_folder = r'C:/Program Files (x86)/RuBib/examples/' #r'd:/temp/rubib/examples/'   #os.getcwd()
        self.cwd = os.getcwd()
        
        self.parent.title('{progname} {version}'.format(version=self.version,
                          progname=self.progname))
#        self.parent.iconbitmap(r'd:\temp\bibref\car.ico')
#        self.parent.iconbitmap(os.getcwd()+'\car.ico')
        self.parent.iconbitmap(r'./materials/car.ico')

        # Создание поисковой строки               
        self.sentvar = StringVar()
        self.sent = Entry(self.parent, textvariable=self.sentvar, width=int(N)) 
        self.sent.grid(row=0, column=1) #, columnspan=2)
        # Кнопка Поиска        
        self.search_but = Button(self.parent, text=u'Искать',
                               command=self.Searchbib, width=LN)
        self.search_but.grid(row=0, column=2)# 3, columnspan=1, rowspan=2)

        self.search_lab = Label(self.parent, width=N, text=u"Поиск в : %s" % self.search_folder, 
                    anchor=E, justify=RIGHT, font=p14)
        self.search_lab.grid(column=0, row=0, sticky='e')

        # Labels для полей атрибутов        
        ents_lab = Label(self.parent, text=u'Поля атрибутов',
                         font=tahoma_bold, justify=tk.CENTER)
        ents_lab.grid(column=1, row=1)
        ents2_lab = Label(self.parent, text=u'Атрибуты bib-файла', 
                          font=tahoma_bold, justify=tk.CENTER)
        ents2_lab.grid(column=2, row=1)        

        for i, key in enumerate(dkeys):
            lab = Label(self.parent, width=LN, text=self.dic[key][0], 
                        anchor=W, justify=LEFT)
            lab.grid(column=2, row=self.dic[key][1], sticky='w')
            str_var = StringVar()
            ent = Entry(self.parent, width=N, textvariable=str_var)
            ent.grid(column=1, row=self.dic[key][1], columnspan=ent_colspan)

            self.dic[key].append(lab)
            self.dic[key].append(str_var)            
            self.dic[key].append(ent)

        # Кнопки
        self.but_save = Button(self.parent, width=N-10, text=u'Убрать из списка',
                               command=self.Removebibfromlist)
        self.but_save.grid(row=21, column=0, columnspan=1, rowspan=2)
                
        self.but_save = Button(self.parent, text=u'Сохранить правку',
                               command=self.Revisbib)
        self.but_save.grid(row=21, column=2, columnspan=1, rowspan=2)
        
        # Список bib-файлов
        lb_lab = Label(self.parent, text=u'Список bib-файлов', width=N,
                       font=tahoma_bold, justify=tk.CENTER)
        lb_lab.grid(column=0, row=1)

        self.lb = Listbox(self.parent, width=N+10, height=21)
        self.lb.bind("<<ListboxSelect>>", self.Selectbib)
        self.lb.grid(column=0, row=n-1, rowspan=20) #len(self.dic)+2)
        
        # Создание выпадающего меню выбора стиля цитирования
        self.csv = StringVar()
        self.csv.set(u"ГОСТ")   # Начальное значение
#        self.csv.set(u"Метеорология и гидрология")        
        self.csom = tk.OptionMenu(self.parent, self.csv, *self.journals_list, 
                                  command=self.citebib_dialog) #Citebib)
        self.csom.config(width=N-10)
        self.csom.grid(row=21, column=1, columnspan=1, rowspan=2, sticky='ew')

        butlabels = [u'', u'Стиль цитирования', u'']
        for i, text in enumerate(butlabels):
            lab = Label(self.parent, text=text,
                         font=tahoma_bold, justify=tk.CENTER)
            lab.grid(column=i, row=20)

        # Создание поля библиографической ссылки

        self.ctext = Text(font=('times',12),
                          width=N*2, height=5, wrap=tk.WORD)
        self.ctext.grid(row=n+25, columnspan=3)

        self.biblio_lab = Label(self.parent, justify=tk.CENTER, anchor='w', 
                                width=N+30, font=tahoma_bold,
                                text=u'Библиографическая ссылка согласно "%s"' % self.csv.get())
        self.biblio_lab.grid(column=0, row=n+24, columnspan=2)   

        # Создание Меню

        menubar = Menu(self.parent)
        self.parent.config(menu=menubar)
        
        fileMenu = Menu(menubar)       
                   
        fileMenu.add_command(label=u"+ Добавить bib-файл", underline=0, 
                             command=self.openbib_dialog)                
        fileMenu.add_command(label=u"> Открыть список bib", underline=0, 
                             command=self.Openbiblist)
#        fileMenu.add_command(label=u"* Создать список bib", underline=0, 
#                             command=self.Createbiblist)
        fileMenu.add_separator()                             
        fileMenu.add_command(label=u"Выход", underline=0, 
                             command=lambda: self.parent.destroy())
        menubar.add_cascade(label=u"Файл", menu=fileMenu)

        reMenu = Menu(menubar)
        menubar.add_cascade(label=u"Правка", underline=0, menu=reMenu) 
        reMenu.add_command(label=u"* Создать новый bib-файл", underline=0, 
                             command=self.Createbib)
        reMenu.add_command(label=u"- Очистить список bib файлов", underline=0, 
                     command=self.Clear_all)   

        citeMenu = Menu(menubar)   
        menubar.add_cascade(label=u"Экспорт", underline=0, menu=citeMenu) 
        citeMenu.add_command(label=u"Экспорт в docx", command=self.Exportbib2docx)
        citeMenu.add_command(label=u"Сохранить bib-список в txt", command=self.Savebiblist)

        searchMenu = Menu(menubar)   
        menubar.add_cascade(label=u"Поиск", underline=0, menu=searchMenu)
        searchMenu.add_command(label=u"? Найти все bib-файлы в...", underline=0, 
                             command=self.Findbibs_in_1_folder)
        searchMenu.add_command(label=u">< Изменить папку поиска", 
                               command=self.Switch_searchfolder)
     #   searchMenu.add_command(label=u"> Изменить папку поиска", 
     #                          command=self.Searchbib)

        
        infoMenu = Menu(menubar)   
        menubar.add_cascade(label=u"Справка", underline=0, menu=infoMenu) 
        infoMenu.add_command(label=u"Документация", command=self.HelpOffline)
        infoMenu.add_command(label=u"О программе", command=self.Info)

    def Createbib(self):
        '''
        '''

        self.clear_data()   # clear Entries

        ftypes = [('Bibtex files', '*.bib'), ('All files', '*')]
        self.fl = tkFileDialog.asksaveasfilename(defaultextension=".bib", 
                                           filetypes=ftypes,
                                           title="Сохраните новый bib файл")
        
#        with open(self.fl, 'w') as f:
#            pass
#        f.close()
        if(self.fl == None or len(self.fl) == 0):
            return
        if(self.fl not in self.biblist):
            self.biblist.append(self.fl)
            self.lb.insert(END, self.fl)

        self.dbib = BibDatabase()
        self.dbib.get_entry_list().append({})

        fllist = self.fl.split('/')
        sfl = fllist[-1]
        sfl = sfl.split('.')
        sfl = sfl[0]

        self.dic['ENTRYTYPE'][4].insert(0, u'article')
        self.dic['ID'][4].insert(END, sfl)

        self.Revisbib()


    def openbib_dialog(self):
        '''
        Opens bib file using GUI interface & adds it to biblist
        '''
     
        ftypes = [('Bibtex files', '*.bib'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes=ftypes,
                                title=u"Выберите bib файл")
        self.fl = dlg.show()
        
        self.Openbib()


    def Openbib(self):

        print (u'Открываю файл {}...'.format(self.fl))
                
        if (self.fl != '' and (self.fl not in self.biblist)):

            ss = self.fl.split(r'/')
            sfl = ss[-1]
            print sfl

            items = self.lb.get(0, END) 
            flag = True
            for i in items:
                if i == ss[-1]: 
                    flag = False
                    break
            if (flag):
                self.lb.insert(END, self.fl)
                self.biblist.append(self.fl)
#            try:
#                self.dbib = {}
#            finally:
#                pass
            self.dbib = {}

        else:
            print(u'Ошибка!')

        self.refresh_biblist()


    def Openbiblist(self):
        '''
        Opens predefined list with bib-files
        '''
        
        self.Clear_all()
        
        ftypes = [('Txt files', '*.txt'), ('DAT files', '*.dat'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes=ftypes, 
                                title=u"Выберите список bib файлов")
        self.fl = dlg.show()

        if (self.fl != ''):
# Clear biblist
            if(self.biblist):
#                for bib in self.biblist:
                self.lb.delete(0, END)
                self.biblist = []

            self.clear_data()
            f = open(self.fl, 'r')
            templist = []
            for bib in f:
                templist.append(bib.strip())
            f.close()
            templist = list(set(templist))
            templist.sort()
            self.biblist = templist
            
            for bib in self.biblist:
                self.lb.insert(END, bib)
        
        self.refresh_biblist()

    def Selectbib(self, val):
        '''
        После выбора файла, отображается его информация в полях и образце
        цитирования
        '''

        self.clear_data()
        
        try:
            sender = val.widget
            idx = sender.curselection()
            value = sender.get(idx)
        except:
            tkMessageBox.showwarning(u"Ошибка", u"Список файлов пуст. Добавьте bib-файл \n (Файл -> + Добавить bib-файл)")

        finally:
            if(self.biblist):
                self.fl = value
                with open(value) as bibtex_file:
                    self.dbib = bibtexparser.load(bibtex_file)
    #            self.make_own_id()
        #        if ('ID' in self.dbib):
        #            self.dbib.get_entry_list()[0]['ID'] = 
                    
    #            print self.dbib.get_entry_list()
        
                print (u'Choosen ... %s' % (self.fl))
                if(self.dbib.get_entry_list()):
                    keys = self.dbib.get_entry_list()[0].keys()
        #            print self.dic
                    for key in keys:
                        if(key in self.dic):
        #                    print key
                            obj = self.dic[key][4]   #  obj = self.dic_ent[key]
                            ss = self.dbib.get_entry_list()[0][key]
         #                   print key, ss, obj
                            if(type(ss) != unicode): ss = unicode(ss)                  
                            obj.insert(END, ss)
                            self.dic[key][3].set(ss)   # self.dic_var[key].set(ss)
            #                self.dic_lab[key].config(font=("Times", 10, "bold"))
    #                        self.dic[key][2].config(fg='red')   # self.dic_lab[key].config(fg='red')
                    cur_entry = self.dbib.get_entry_list()[0]['ENTRYTYPE']
                    entrylist = ['article', 'book', 'inproceedings']
                    for i, se in enumerate(entrylist):
                        if(i == 0):
                           templist = ['author', 'title', 'journal', 'year',
                                       'volume', 'number', 'pages', 'ENTRYTYPE']
                                      
                        elif(i == 1):
                            templist = ['author', 'title', 'publisher', 'address', 
                                        'year', 'pages', 'ENTRYTYPE']
                        else:
                            templist = ['author', 'title', 'journal', 'year',
                                        'volume', 'number', 'pages', 'ENTRYTYPE']
                        c = (cur_entry == se.lower() or cur_entry == se.upper() or
                            cur_entry == se.capitalize())
                        if(c):                        
                            for key in templist:
                                self.dic[key][2].config(fg='red')
                    
                    self.parent.update()
            
                    if (self.biblist):
                        self.citebib_dialog(val)
        #                self.Citebib()
                else:
        #                self.dbib = BibDatabase()
                    print (u'Opened bib-file is empty!')
                    self.dbib.get_entry_list().append({})
                    self.dbib.get_entry_list()[0]['ENTRYTYPE'] = u'article'
                    self.dbib.get_entry_list()[0]['ID'] = u'temp'



#    self.index = int(self._Listbox.curselection()[0])
#        except:
#            tkMessageBox.showwarning(u"Ошибка", u"Список файлов пуст. Добавьте bib-файл \n (Файл -> + Добавить bib-файл)")
#        finally:
#            pass
#            tkMessageBox.showwarning(u"Ошибка", u"Список файлов пуст. Добавьте bib-файл \n (Файл -> + Добавить bib-файл)")
       
    
    def Citebiblist(self):
        '''
        '''
        self.citelist = []

        for bib in self.sorted_biblist:
            with open(bib) as bibtex_file:
                self.dbib = bibtexparser.load(bibtex_file)
                if(self.dbib.get_entry_list()):
                    self.Citebib()
#                    print self.citetext
                    self.citelist.append(self.citetext)

    def citebib_dialog(self, val):
        '''
        '''

        cs = u'Библиографическая ссылка согласно "%s" ' % self.csv.get()
        self.biblio_lab.config(text=cs)

        if(self.biblist): self.Citebib()
    
    def Citebib(self):
        '''
        '''
        self.authors_parser()
#        keys = self.dbib.get_entry_list()[0].keys()
        self.citestyle = self.csv.get()
        self.citetext = ''
        
        self.Citetemplate()
#        print 'Журнал %s', self.citestyle
        '''
        if(self.citestyle == u'Метеорология и гидрология'):
            self.temp_M_and_H_rus()
        elif(self.citestyle == u'ГОСТ 7.1-2003'):
            self.temp_GOST_7_1_2003_rus()
        else:
            self.citetext = u"Для выбранного журнала нет готового шаблона!"
        '''
        
        self.update_citetext()
    

    def update_citetext(self):
        self.ctext.delete(1.0, END)
        self.ctext.insert(END, '%s \n' % self.citetext)


    def Citetemplate(self):
        '''
        '''
        etype = self.dbib.get_entry_list()[0]['ENTRYTYPE'].lower()
        entdb = self.dbib.get_entry_list()[0]

        if(self.citestyle == u'ГОСТ'):
            from templates.GOST_7_1_2003_rus_2 import temp
        elif(self.citestyle == u'Метеорология и гидрология'):
            from templates.Meteorology_and_Hydrology_rus_2 import temp            
        else:
            from templates.GOST_7_1_2003_rus_2 import temp

        self.citetext = temp(etype, entdb, self.authorlist)


    def HelpOffline(self):
        '''
        Открывает документацию, хранящуюся в manual.pdf
        '''
        try:
            os.system("start " + r'./materials/manual.pdf')
        except:
            s = u'Ошибка! Похоже файл с документацией куда-то потерялся!'
            tkMessageBox.showwarning(u"Возникла ошибка!", s)
            

#    def HelpOnline(self):
#        '''
#        '''
#        
##        helpsite = r'http://progeoru.blogspot.ru/'
#        helpsite = r'http://progeoru.blogspot.ru/2015/12/help.html'
#
#        try:
#            webbrowser.open(helpsite)
##            webbrowser.open_new(r'file://./materials/manual.pdf')
#        except:
#            s = u'\t Возникла ошибка при доступе в интернет через браузер! \n\
#            \n\
#            \t Для получения помощи посетите страницу: \n \
#            \n\
#            \t %s' % helpsite
#            tkMessageBox.showwarning(u"Помощь в пути!", s)

            
    def Info(self):
        
        py_version = sys.version.split(' ')[0]
        
        longstring = u'\t {progname} - простой и функциональный \n\
        \t библиографический менеджер bib-файлов \n\
        \n\
        \t Версия: {version} \n\
        \n\
        \t Автор: П.А. Шабанов \n\
        \n\
        \t Email: pa.shabanov@gmail.com \n\
        \n\
        \t Блог: http://progeoru.blogspot.ru \n\
        \n\
        \t Реализовано на: python {py_version} \n\
        \n\
        \t Copyright(c) 2015. Все права защищены.'.format(version=self.version,
                                                   progname=self.progname,
                                                   py_version=py_version)
        tkMessageBox.showwarning(u"О программе %s" % self.progname,
                                 longstring)
       

    def Exportbib2docx(self):

        ftypes = [('Docx files', '*.docx'), ('All files', '*')]

        f = tkFileDialog.asksaveasfilename(defaultextension=".docx",
                                           filetypes=ftypes,
                                           title="Экспорт в docx")
        print f, type(f)
        if (f == None or len(f) == 0): # asksaveasfile return `None` if dialog closed with "cancel".
            return
#        s = r'%s\default.docx' % self.cwd  
#        s = r'd:\temp\bibref\tempdocf.docx'
        s = r'c:\Users\pasha\Anaconda\Lib\site-packages\docx\templates\default.docx'
        s = r'./materials/default.docx'
        document = Document(s)

#        from docx.shared import Pt
#        run = document.add_paragraph()
#        font = run.font
#        font.name = 'TimesNewRoman'
#        font.size = Pt(14)
#        p = document.add_paragraph()
#        p.add_run(u'Список литературы').bold = True

        self.Sortbibs()

        self.Citebiblist()
 
        for cite in self.citelist:
#            self.Cite()
            p = document.add_paragraph(cite, style='List Number')

#        document.add_page_break()
#        ss = f.split(r'/')
#        document.save(ss[-1])
        document.save(f)


    def Sortbibs(self):
        '''
        '''
        self.sorted_biblist = []
        strdic = {}        
        for bib in self.biblist:
#            print 'bib', bib
            with open(bib) as bibtex_file:
                self.dbib = bibtexparser.load(bibtex_file)
            self.authors_parser()
#            print self.cite_authors
#            'Богданова, Э Г and Гаврилова, С Ю'
            if('author' in self.dbib.get_entry_list()[0]):
                if (len(self.dbib.get_entry_list()[0]['author']) == 0):
                    firstline = self.dbib.get_entry_list()[0]['title']
                else:
                    firstline = self.cite_authors
                    #                firstline = self.cite_authors

            else:
                firstline = self.dbib.get_entry_list()[0]['title']
            strdic[bib] = '%s, %s' % (firstline, self.dbib.get_entry_list()[0]['year'])
#            print 'strdic:', strdic[bib]

        x = sorted(strdic.items(), key=operator.itemgetter(1))
        clist = []
        for i in x:
            clist.append(i[-1])
            self.sorted_biblist.append(i[0])

        print clist
        for i, string in enumerate(clist):
            if(re.findall(r'[АЯаяЁё]+', string[0].encode('utf8'))):
                self.sorted_biblist = (self.sorted_biblist[i:] + 
                                      self.sorted_biblist[:i])
                break
        
    
    def Savebiblist(self):
#                ftypes = [('Txt files', '*.txt'), ('DAT files', '*.dat'), ('All files', '*')]
#        dlg = tkFileDialog.Open(self, filetypes=ftypes, 
#                                title=u"Выберите список bib файлов")
        ftypes = [('Txt files', '*.txt'), ('DAT files', '*.dat'), ('All files', '*')]
        f = tkFileDialog.asksaveasfile(mode='w', defaultextension=".txt",
                                       filetypes=ftypes, 
                                       title=u"Сохраните список bib файлов"
        )
        if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        print 'BIBlist:', self.biblist
        for bib in self.biblist:
            f.write('%s\n' % bib)
        f.close() # `()` was m        
        
        
    def Cite_style(self):
        
        print 'Label: %s' % self.citelabel
        self.csv.set(self.citelabel)
#        self.cs_lab.set(label)


    def clear_data(self):
        u'''
        Удаляет с главного окна занчения в полях атрибутов, подсветку атрибутов,
        поле библиографической ссылки.
        '''
        
        for key in self.dic:
            obj = self.dic[key][4]
            obj.delete(0, END)
            self.dic[key][3].set('')
            self.dic[key][2].config(fg='black')
            
        self.parent.update()                    
        self.ctext.delete(1.0, END)

    
    def refresh_biblist(self):

        # Очистка 
        for i in xrange(len(self.biblist)):
            self.lb.itemconfigure(i, background='#ffffff')
            
        # Раскраска в зебру
        for i in xrange(0, len(self.biblist), 2):
            self.lb.itemconfigure(i, background='#f0f0ff')        


    def Removebibfromlist(self):
        '''
        '''        
#        print len(self.biblist)
        
        if(self.biblist):
            idx = self.lb.curselection()
#            print 'idx', idx
            self.lb.delete(idx, idx)
            self.biblist.pop(idx[0])
            
            for key in self.dic:
                obj = self.dic[key][4]
                obj.delete(0, END)
            self.ctext.delete(1.0, END)

#        print len(self.biblist)
        self.refresh_biblist()
        self.clear_data()



    def Clear_all(self):
        '''
        '''
        print 'BIBLIST in CLEAR ALL', self.biblist
        try:
            self.lb.delete(0, END)
            self.biblist = []

            for key in self.dic:
                obj = self.dic[key][4]
                obj.delete(0, END)
            self.ctext.delete(1.0, END)
            
            self.clear_data()
        finally:
            pass
                        

    def Revisbib(self):
        '''
        Helps to revise the bib file and correct mistakes
        '''
#        keys = self.dbib.get_entry_list()[0].keys()
#        
#        for key in keys:
#            if(key in self.dic): 
#                obj = self.dic[key][4]
#                ss = obj.get()
##                if(type(ss) is unicode):            
##                    ss = ss.encode('utf-8')
#                if(type(ss) is str):            
#                    ss = unicode(ss)
#                self.dbib.get_entry_list()[0][key] = ss
#                print type(ss)
#        self.refresh_bib()
        
#        keys = self.dbib.get_entry_list()[0].keys()
        if(self.biblist):
            for key in self.dic:
                obj = self.dic[key][4]
                ss = obj.get()
    #            if(ss == '' or ss == None):
    #                pass
                    # удаление записи
    #                if (key in self.dbib): del self.dbib.get_entry_list()[0][key]
    #            else:
                if(type(ss) is str): ss = unicode(ss)
                self.dbib.get_entry_list()[0][key] = ss                  
    
                    
            self.refresh_bib()

    def refresh_bib(self):
        '''
        writer = BibTexWriter()
        try:
            with open(self.fl, 'w') as bibfile:
                bibfile.write(writer.write(self.dbib))
        finally:
            pass
        '''
        bibtex_str = bibtexparser.dumps(self.dbib)
        try:
            with open(self.fl, 'w') as bibfile:
#                bibfile.seek(0)
#                bibfile.truncate()
                bibtex_str = bibtex_str.encode('utf-8')
                bibfile.write(bibtex_str)
            self.Citebib()             
            self.update_citetext()

        finally:
            pass

    def Searchbib(self, folder=r'D:/', fileformat = "bib", findall=False):
        '''
        '''        
#        zagran = {'A':u'А','B':u'Б','C':'','D':'','E':'', 'F':'','G':'',
#                  'H':'','I':'','J':'','K':'','L':'','M':'','N':'','O':'','P':'',
#                  'Q':'','R':'', 'S':'', 'U':'', 'V':'', 'W':'', 'X':'', 'Y':'', 'Z':''}


        emptyresult = u'По запросу ничего не найдено' 
        fileformat = "bib"
        if self.search_folder: folder = self.search_folder

        '''
                ilist = []
        if(len(self.sent.get()) != 0):
            tofind = self.sent.get()
        else:
            tofind = ''
        for root, dirs, files in os.walk(folder):
            for f in files:
                if f.endswith('.%s' % fformat):
                    ilist.append(os.path.join(root, f))
        ilist.sort()
        '''

        ilist = []
        s = self.sent.get()
        if(self.sent.get() == emptyresult): return


#            if(findall):
#                pass            
#            else:
#                return
        
        self.search_but.config(state=tk.DISABLED)
        sep = ' '
        for i in [u'+', u'&']:
            if (i in s): 
                sep = i
                
        k = s.split(sep)
        for root, dirs, files in os.walk(folder):
            for f in files:
                if f.endswith('.%s' % fileformat):
                    flags = []
                    for s in k:
                        s = s.strip()
                        flag = self.scope(root, f, s)
                        flags.append(flag)
                    if(sep == '+'):
                        for i, s in enumerate(k):
                            flag = flags[i]
                            if(flag):
                                fullpath = os.path.join(root, f)  
                                ilist.append(fullpath)
                    elif(sep == '&'):
                        print flags
                        if(all(flags)):
                            fullpath = os.path.join(root, f)  
                            ilist.append(fullpath)
                    else:
                        if(all(flags)):
                            fullpath = os.path.join(root, f)  
                            ilist.append(fullpath)
                                        
        self.search_but.config(state=tk.NORMAL)
                        
        if not ilist:
            ilist = emptyresult
            self.sentvar.set(ilist)

#            self.sent.insert(0, ilist)
            return

        ilist.sort()
        
#        self.Clear_all()
        self.clear_data()
        
        for self.fl in ilist:
            if not self.fl in self.biblist:
                self.lb.insert(END, self.fl)
                self.biblist.append(self.fl)
        
        self.refresh_biblist()
        
#        self.search_but.config(state=tk.NORMAL)

    def scope(self, root, f, s, flag=False):
        '''
        Just searches in file
        '''
        fullpath = os.path.join(root, f)  
        slate = self.translation_rus_2_eng(s)  # rus                      
        # Easy way 
#                    print 's', s, 'late:', slate #, f
        if((s in f) or (slate in f)):
            flag = True
        # Search in authors
        else:
#                        print s, slate
            with open(fullpath) as bibtex_file:
                self.dbib = bibtexparser.load(bibtex_file)
                dic = self.dbib.get_entry_list()[0]
                
#                print 's', s, s.isdigit()
                if s.isdigit():
                    y = dic['year']
#                    print y, s
                    if ((s == y) or (s in y)):
                        flag = True
                    else:
#                        print s, y
                        flag = False
                    return flag
                
                else:
                    keys = dic.keys()
                    for key in keys:
                        if(key == 'author'):
                            self.authors_parser()
                            for man in self.authorlist:
                                lastname = man[0].lower()
            #                                print man[0]
                                tman = self.translation_rus_2_eng(lastname)
            #                    print '--vvvvv--'
            #                    print lastname, tman, s, slate
            #                    print '---^^^^^^--'
                                if((s in lastname) or (slate in lastname) or
                                (s in tman) or (slate in tman)):
                                    flag = True
#                        elif(key == 'title'):
                        else:
                            ss = dic[key].lower()
                            if (s in ss): flag=True
                                
        return flag
    
    def Switch_searchfolder(self):
        
        folder = tkFileDialog.askdirectory()
#        print 'choosed folder', folder
        if(len(folder) == 0):
            pass
        else:
            self.search_folder = folder
        self.search_lab.config(text=u'Поиск в: %s' % self.search_folder)


    def Findbibs_in_1_folder(self):
        '''
        '''

        self.Clear_all()
        folder = tkFileDialog.askdirectory()

        if(len(folder) == 0):            
            return
        bibfiles = os.listdir(folder)
        purebib = []
        for bib in bibfiles:
            if ('.bib' in bib or u'.bib' in bib): purebib.append(bib)

        if(purebib):
            for bib in purebib:
                bib = r'%s/%s' % (folder, bib)
                print bib
                self.fl = bib
                self.Openbib()
        else:
            s = u'В выбранной папке "%s" не найдено ни одного bib-файла!' % folder
            tkMessageBox.showwarning(u"Нулевой поиск!", s)         



    def make_own_id(self):
#        if ('ID' in self.dbib.get_entry_list()[0]):
        try:
            fllist = self.fl.split('/')
            sfl = fllist[-1]
            sfl = sfl.split('.')
            sfl = sfl[0]
            print self.dbib.get_entry_list()
            self.dbib.get_entry_list()[0]['ID'] = sfl
        finally:
            pass

    def translation_rus_2_eng(self, s):
        '''
        zagran = {u'а' : 'a', u'б' : 'b', u'в' : 'v', u'г' : 'g', u'д' : 'd', 
                u'е' : 'e', u'ё' : 'e', u'ж' : 'zh', u'з' : 'z', u'и' : 'i', 
                u'й' : 'i', u'к' : 'k', u'л' : 'l', u'м' : 'm', u'н' : 'n', 
                u'о' : 'o', u'п' : 'p', u'р' : 'r', u'с' : 's', u'т' : 't', 
                u'у' : 'u', u'ф' : 'f', u'х' : 'kh', u'ц' : 'tc', u'ч' : 'ch',
                u'ш' : 'sh', u'щ' : 'shch', u'ы' : 'y', u'э' : 'e',
                u'ю' : 'iu', u'я' : 'ia'}  
        if(re.findall(r'[АЯаяЁё]+', s.encode('utf8'))):
            zdic = zagran
        else:
            zdic = {v: k for k, v in zagran.items()}
            
        z = s.lower()
        trans = ''
#        print z
        for i in z:
            if(i in zdic):
                trans += zdic[i]
        trans = trans.strip()
#        print trans
        return trans
        '''
        zagran = {u'а' : 'a', u'б' : 'b', u'в' : 'v', u'г' : 'g', u'д' : 'd', 
        u'е' : 'e', u'ё' : 'e', u'ж' : 'zh', u'з' : 'z', u'и' : 'i', 
        u'й' : 'i', u'к' : 'k', u'л' : 'l', u'м' : 'm', u'н' : 'n', 
        u'о' : 'o', u'п' : 'p', u'р' : 'r', u'с' : 's', u'т' : 't', 
        u'у' : 'u', u'ф' : 'f', u'х' : 'kh', u'ц' : 'tc', u'ч' : 'ch',
        u'ш' : 'sh', u'щ' : 'shch', u'ы' : 'y', u'э' : 'e',
        u'ю' : 'iu', u'я' : 'ia'}  

        zdic = {v: k for k, v in zagran.items()}
        zdic['e'] = u'е'

        z = s.lower()
    
        if(re.findall(r'[АЯаяЁё]+', s.encode('utf8'))):
#            print 'FFFF'
            trans = ''
            for i in z:
                if i in zagran:
                     trans += zagran[i]
            trans = trans.strip()
            return trans
        else:
            vals = zagran.keys()
            vals = zagran.values()
            twosym = []
            for let in vals:
                if(len(let) > 1):
                    twosym.append(let)
            onesym = list(set(vals) - set(twosym))
    #        print  type(twosym)
            twosym.sort(reverse=True)
    #        print type(onesym), type(twosym)
            newdic = twosym + onesym
        # 'shch', 'sh', 'kh', 'zh', 'tc' 
    #        print newdic
            for i in newdic:
    #            print i, 
                z = z.replace(i, zdic[i])
            return z #trans


    def authors_parser(self):
        '''
        '''
        key = u'author'
#        authors = self.dbib[key]
#        return authors

        self.authorlist = []
        ilist = []
#        print self.dbib.get_entry_list()[0].keys()
        if (key in self.dbib.get_entry_list()[0].keys()):
            authors = self.dbib.get_entry_list()[0][key]
#        print type(names), names
            authors = authors.split('and')
            for i, man in enumerate(authors):
                name = ''
                ss = man.split(',')
#                for s in ss:                
#                    print 'ss:', s.encode('utf8')
                lastname = (ss[0].strip()).capitalize()   # Last name
                abr = ss[-1].strip().title()
#                print lastname
                name += lastname
                if(len(ss) > 1):
                    ss = ss[1:]
                    ss = ss[0].strip()
##                    print 'short', ss
                    abr = ss.split(' ')
                    for i in abr:
                        j = i[0].upper()
                        s = '%s.' % j
#                        print lastname, s
                        name += ' %s' % s
                self.authorlist.append([lastname, abr])
#                self.authorslist.append(name)
                ilist.append(name)
                
        else:
            pass

        self.cite_authors = ''
#        print 'AUTHOR LIST', self.authorlist
        for man in ilist:
            self.cite_authors += '%s, ' % man
        self.cite_authors = self.cite_authors[:-2]      
        

def main():
  
    root = Tk()
#    root.geometry("250x150+300+300")
#    root.geometry('%dx%d%+d%+d' % (760, 520, 50, 50))
#    root.iconbitmap('car.ico')
#    root.tk.call('wm','iconphoto', root._w, tk.PhotoImage(file='mice.jpg'))
    RuBib(root)
    root.mainloop()  

if __name__ == '__main__':
    main()  