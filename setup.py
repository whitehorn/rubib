# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 10:19:59 2015

@author: pasha
"""

#from distutils.core import setup
#import py2exe

#setup(windows=[r'bibref_main.py'])

#data_files=['C:\Users\pasha\Anaconda\pkgs\tk-8.5.18-vc9_0\Library\lib\tcl8.5\init.tcl'],
#setup(console=['bibref_main.py'])

'''
import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "guifoo",
        version = "0.1",
        description = "My GUI application!",
        options = {"build_exe": build_exe_options},
        executables = [Executable("bibref_main.py", base=base)])
'''
import os
from distutils.core import setup
import py2exe

#data_files=[r'C:\Users\pasha\Anaconda\pkgs\tk-8.5.18-vc9_0\Library\lib\tcl8.5\init.tcl', 
#r'D:/Program Files (x86)/Python27/Lib/site-packages/docx-template/*']


#path = r'd:/temp/rubib'
path = os.getcwd()

Mydata_files = []
for files in os.listdir(path + r'/materials/'):
    f1 = os.getcwd() + '/materials/' + files
    if os.path.isfile(f1): # skip directories
        f2 = 'materials', [f1]
        Mydata_files.append(f2)

#Examples = []
for files in os.listdir(path + r'/examples/'):
    f1 = os.getcwd() + '/examples/' + files
    if os.path.isfile(f1): # skip directories
        f2 = 'examples', [f1]
        Mydata_files.append(f2)

#        Examples.append(f2)

setup( name = "RuBib",
       version = "1.1.0",
       description = u"Простой и функциональный библиографический менеджер bib файлов",
       data_files = Mydata_files,
       windows=[{'script': 'rubib_main.py', "icon_resources": [(1, "./materials/car.ico")]}],
       options={'py2exe': 
        {   
            'includes': ['lxml.etree', 'lxml._elementpath', 'gzip'],
        }
    }
    
)
