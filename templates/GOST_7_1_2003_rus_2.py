# -*- coding: utf-8 -*-
"""
Created on Tue Dec 01 21:49:20 2015

@author: pasha
"""

import re, time

def parser2str(xmen, debug=False):
    '''
    '''
    pass
    print xmen
    authors = ''
    for man in xmen:
        lastname = man[0].strip()
        abr = man[-1]
        firstname = ''
        for ini in abr:
            firstname += '%s. ' % ini[0].title()
        firstname = firstname.strip()

        author = '%s %s' % (lastname, firstname)
        authors += '%s, ' % author    

    authors = authors[:-2]
    
    return authors
    
def temp(etype, entdb, authorlist, debug=False):
    '''
    '''
    if(re.findall(r'[АЯаяЁё]+', entdb['title'].encode('utf8'))):
#        language = 'rus'
        number_s = u'№'
#        volume_s = u'вып.'
        volume_s = u'т.'
        pages_s = u'C.'
    else:
#        language = 'eng'
        number_s = u'№'
#        volume_s = u'вып.'
        volume_s = u'vol.'
        pages_s = u'p.'    
    
    if(etype == 'article'):
        tlist = ['title', 'author', 'journal', 'year', 'volume', 
                    'number', 'pages']
        templist = {'author' : {'s1' : ' // '},
                    'title' :  {'s2' : ' / '},
                    'journal': {'s3' : '. - '},
                    'year' : {'s4' : '. - '},
                    'volume' : {'s5' : '. - '}, 
                    'number' : {'s6' :'. - '},
                    'pages' : {'s7': ''}}

    elif(etype == 'book'):    

        tlist = ['title', 'author', 'publisher', 'address', 'year', 'pages']
        templist = {'title' :  {'s2' : ' / '},
                    'author' : {'s1' : '. - '},
                    'publisher' : {'s6' :' '},
                    'address': {'s3' : ', '},
                    'year' : {'s4' : '. - '},
                    'pages' : {'s7': ''}}     
    else:
        ss = time.strftime("%H:%M:%S - %d/%m/%Y")
        s = (u'Данный тип данных (%s) находится на стадии разработки %s' % 
        (etype, ss))
        return s
        
    itemp = u''    
    for i in tlist:
        if(i in entdb):
            if(len(entdb[i]) == 0):
                pass
            else:
                itemp += u'{%s}{%s}' % (i, templist[i].keys()[0])
#            itemp += u'{%s}{%s}' % (i, templist[i].keys()[0])
            
    dic = {}
    for i in templist:
        dic[i] = unicode(i)

    for key in dic:
        if (key in entdb):
            ss = entdb[key]
    #        print 'key, ss:', key, ss 
            if(ss == '' or ss == None):
                #ss = '???'
                pass
            else:
                if(key == 'journal' or 'title'): 
                    ss = entdb[key]
                    if(ss[0].islower()):
                        z = ss[0].upper()
                        ss = z + ss[1:]
                        
                if(key == 'number'): ss = '%s %s' % (number_s, entdb[key])
                if(key == 'volume'): ss = '%s %s' % (volume_s, entdb[key])
                if(key == 'pages'):
                    p = re.compile(r'[0-9]+')
                    pp = p.findall(entdb[key])
                    if(etype == 'article'):                        
                        if(len(pp) == 1):
                            ss = '%s %s' % (pp[0], pages_s)
                        elif(len(pp) >= 2):
                            ss = '%s %s-%s' % (pages_s, pp[0], pp[1])
                        else:
                            ss = '%s %s' % (pages_s, '')
                    elif(etype == 'book'):
                        ss = '%s %s' % (pages_s, pp[0])
                    else:
                        ss = '%s %s' % (pages_s, pp[0])
                    
                if(key == 'author'): ss = parser2str(authorlist)
#                    ss = authors
            dic[key] = ss
    
    for i in tlist:
    
        k= templist[i].keys()[0]
        dic[k] = templist[i][k] 
    
    s = itemp.format(**dic)
    s = s.strip() + '.'
 
#        s = u'''Такой тип данных (%s) для выбранного стиля цитирования 
#находится в разработке''' % etype
    
    return s